/**
 * Copyright 2011 Joao Miguel Pereira
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package eu.jpereira.trainings.designpatterns.creational.abstractfactory;

public class Report {

	private ReportHeader header;
	private ReportBody body;
	private ReportFooter footer;

	public Report(ReportFactory reportFactory) {
		this.header = reportFactory.getHeader();
		this.body = reportFactory.getBody();
		this.footer = reportFactory.getFooter();
	}

	public ReportHeader getHeader() {
		return header;
	}

	public ReportBody getBody() {
		return body;
	}

	public ReportFooter getFooter() {
		return footer;
	}
}
