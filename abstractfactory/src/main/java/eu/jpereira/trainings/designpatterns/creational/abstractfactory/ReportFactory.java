package eu.jpereira.trainings.designpatterns.creational.abstractfactory;

public interface ReportFactory {

    ReportBody getBody();

    ReportFooter getFooter();

    ReportHeader getHeader();

}
