package eu.jpereira.trainings.designpatterns.creational.abstractfactory.json;

import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportBody;
import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportFactory;
import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportFooter;
import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportHeader;

public class JSONReportFactory implements ReportFactory {

    @Override
    public ReportBody getBody() {
        return new JSONReportBody();
    }

    @Override
    public ReportFooter getFooter() {
        return new JSONReportFooter();
    }

    @Override
    public ReportHeader getHeader() {
        return new JSONReportHeader();
    }

}
