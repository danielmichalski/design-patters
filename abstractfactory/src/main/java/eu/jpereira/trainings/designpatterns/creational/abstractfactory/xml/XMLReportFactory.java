package eu.jpereira.trainings.designpatterns.creational.abstractfactory.xml;

import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportBody;
import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportFactory;
import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportFooter;
import eu.jpereira.trainings.designpatterns.creational.abstractfactory.ReportHeader;

public class XMLReportFactory implements ReportFactory {

    @Override
    public ReportBody getBody() {
        return new XMLReportBody();
    }

    @Override
    public ReportFooter getFooter() {
        return new XMLReportFooter();
    }

    @Override
    public ReportHeader getHeader() {
        return new XMLReportHeader();
    }

}
