package eu.jpereira.trainings.designpatterns.creational.builder;

import eu.jpereira.trainings.designpatterns.creational.builder.model.Report;
import eu.jpereira.trainings.designpatterns.creational.builder.model.ReportBody;
import eu.jpereira.trainings.designpatterns.creational.builder.model.SaleEntry;

public class ReportBuilder {
    private final SaleEntry saleEntry;
    private final ReportBody reportBody;

    public ReportBuilder(Builder builder) {
        this.saleEntry = builder.saleEntry;
        this.reportBody = builder.reportBody;
    }

    public static class Builder {
        private SaleEntry saleEntry;
        private ReportBody reportBody;


        public Builder saleEntry(SaleEntry saleEntry) {
            this.saleEntry = saleEntry;
            return this;
        }

        public Builder reportBody(ReportBody reportBody) {
            this.reportBody = reportBody;
            return this;

        }

        public ReportBuilder build() {
            return new ReportBuilder(this);
        }
    }

    public Report getReport() {
        Report report = new Report();
        report.setReportBody(reportBody.buildReportBody(saleEntry));
        return report;
    }


}
