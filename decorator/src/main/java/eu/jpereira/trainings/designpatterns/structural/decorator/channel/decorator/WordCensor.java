package eu.jpereira.trainings.designpatterns.structural.decorator.channel.decorator;


import eu.jpereira.trainings.designpatterns.structural.decorator.channel.SocialChannel;

public class WordCensor extends SocialChannelDecorator {

    public static final String CENSORED_TEXT_REPLACE_BY = "###";

    private String censoredWord;

    public WordCensor(String censoredWord) {
        this.censoredWord = censoredWord;
    }

    public WordCensor(String censoredWord, SocialChannel socialChannel) {
        this(censoredWord);
        this.delegate = socialChannel;
    }

    @Override
    public void deliverMessage(String message) {
        message = message.replace(censoredWord, CENSORED_TEXT_REPLACE_BY);
        delegate.deliverMessage(message);
    }

}
