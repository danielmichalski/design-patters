package eu.jpereira.trainings.designpatterns.structural.facade;

import eu.jpereira.trainings.designpatterns.structural.facade.model.Book;
import eu.jpereira.trainings.designpatterns.structural.facade.model.Customer;
import eu.jpereira.trainings.designpatterns.structural.facade.model.DispatchReceipt;
import eu.jpereira.trainings.designpatterns.structural.facade.model.Order;
import eu.jpereira.trainings.designpatterns.structural.facade.service.*;

public class DefaultBookstoreFacade implements BookstoreFacade {

    private BookDBService bookDBService;
    private CustomerDBService customerDBService;
    private CustomerNotificationService customerNotificationService;
    private OrderingService orderingService;
    private WharehouseService wharehouseService;

    public DefaultBookstoreFacade(BookDBService bookDBService,
                                  CustomerDBService customerDBService,
                                  CustomerNotificationService customerNotificationService,
                                  OrderingService orderingService,
                                  WharehouseService wharehouseService) {
        this.bookDBService = bookDBService;
        this.customerDBService = customerDBService;
        this.customerNotificationService = customerNotificationService;
        this.orderingService = orderingService;
        this.wharehouseService = wharehouseService;
    }

    @Override
    public void placeOrder(String customerId, String isbn) {
        Customer customer = customerDBService.findCustomerById(customerId);
        Book book = bookDBService.findBookByISBN(isbn);
        Order order = orderingService.createOrder(customer, book);
        DispatchReceipt dispatchReceipt = wharehouseService.dispatch(order);
        customerNotificationService.notifyClient(dispatchReceipt);
    }

}
