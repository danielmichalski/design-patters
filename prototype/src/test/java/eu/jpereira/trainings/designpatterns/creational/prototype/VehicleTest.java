package eu.jpereira.trainings.designpatterns.creational.prototype;

import com.google.common.collect.Lists;
import eu.jpereira.trainings.designpatterns.creational.prototype.model.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class VehicleTest {

    @Test
    public void shouldCloneVehicle() throws Exception {
        // given
        Shell shell = new Shell();
        Tire tire = new Tire();
        Window window = new Window();

        Vehicle vehicle = new Vehicle();
        ArrayList<VehiclePart> vehicleParts = Lists.<VehiclePart>newArrayList(shell, tire, window);
        vehicle.setParts(vehicleParts);

        // when
        Vehicle clonedVehicle = (Vehicle) vehicle.clone();

        // then
        assertThat(clonedVehicle, is(notNullValue()));
        assertThat(clonedVehicle.countParts(), is(3));
        assertThat(clonedVehicle.getParts(VehiclePartEnumeration.SHELL).size(), is(1));
        assertThat(clonedVehicle.getParts(VehiclePartEnumeration.SHELL), not(contains(shell)));

        assertThat(clonedVehicle.getParts(VehiclePartEnumeration.TIRE).size(), is(1));
        assertThat(clonedVehicle.getParts(VehiclePartEnumeration.TIRE), not(contains(shell)));

        assertThat(clonedVehicle.getParts(VehiclePartEnumeration.WINDOW).size(), is(1));
        assertThat(clonedVehicle.getParts(VehiclePartEnumeration.WINDOW), not(contains(shell)));

    }
}
