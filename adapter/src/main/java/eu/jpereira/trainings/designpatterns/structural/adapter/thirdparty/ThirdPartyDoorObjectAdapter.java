package eu.jpereira.trainings.designpatterns.structural.adapter.thirdparty;

import eu.jpereira.trainings.designpatterns.structural.adapter.exceptions.CodeMismatchException;
import eu.jpereira.trainings.designpatterns.structural.adapter.exceptions.IncorrectDoorCodeException;
import eu.jpereira.trainings.designpatterns.structural.adapter.model.Door;

public class ThirdPartyDoorObjectAdapter implements Door {

    private boolean isOpen = false;
    private String code = ThirdPartyDoor.DEFAULT_CODE;


    @Override
    public void open(String code) throws IncorrectDoorCodeException {
        if (code.equals(ThirdPartyDoor.DEFAULT_CODE)) {
            isOpen = true;
        } else {
            throw new IncorrectDoorCodeException();
        }
    }

    @Override
    public void close() {
        isOpen = false;
    }

    @Override
    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public void changeCode(String oldCode, String newCode, String newCodeConfirmation) throws IncorrectDoorCodeException, CodeMismatchException {
        if (!code.equals(oldCode)) {
            throw new IncorrectDoorCodeException();
        } else if (!newCode.equals(newCodeConfirmation)) {
            throw new CodeMismatchException();
        } else {
            this.code = newCode;
        }
    }

    @Override
    public boolean testCode(String code) {
        return this.code.equals(code);
    }

}
